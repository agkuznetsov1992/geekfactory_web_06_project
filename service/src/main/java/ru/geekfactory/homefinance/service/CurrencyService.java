package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.service.converter.CurrencyConverter;
import ru.geekfactory.homefinance.service.model.CurrencyModelService;

import java.util.ArrayList;
import java.util.List;

public class CurrencyService {
    CurrencyRepository currencyRepository;
    CurrencyConverter currencyConverter = new CurrencyConverter();

    public CurrencyService() {
        this.currencyRepository = new CurrencyRepository();
    }

    public CurrencyService(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public long save(CurrencyModelService currencyModelService) {
        CurrencyModel convertedModel = currencyConverter.toDao(currencyModelService);

        return currencyRepository.save(convertedModel);
    }

    public List<CurrencyModelService> getTotalCurrencies() {
        List<CurrencyModelService> returnArray = new ArrayList<>();
        List<CurrencyModel> currencyModelList = currencyRepository.findAll();

        for(CurrencyModel x : currencyModelList){
            returnArray.add(currencyConverter.toService(x));
        }

        return returnArray;
    }

    public boolean update(CurrencyModelService currencyModelService) {
        CurrencyModel convertedModel = currencyConverter.toDao(currencyModelService);

        return currencyRepository.update(convertedModel);
    }

    public boolean delete(long id){
        return currencyRepository.delete(id);
    }
}
