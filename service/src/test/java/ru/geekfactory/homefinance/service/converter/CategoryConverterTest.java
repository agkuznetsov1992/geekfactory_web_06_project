package ru.geekfactory.homefinance.service.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.service.model.CategoryTransactionModelService;

public class CategoryConverterTest {
    CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
    CategoryTransactionModelService categoryTransactionModelService = new CategoryTransactionModelService();
    CategoryConverter categoryConverter = new CategoryConverter();

    @BeforeEach
    void prepareModels() {
        categoryTransactionModel.setId(1L);
        categoryTransactionModel.setName("test Category");
        categoryTransactionModelService.setId(1L);
        categoryTransactionModelService.setName("test Category");
    }

    @Test
    @DisplayName("Category Converter to dao Test")
    void convertToDaoTest() {
        Assertions.assertEquals(categoryTransactionModel, categoryConverter.toDao(categoryTransactionModelService));
    }

    @Test
    @DisplayName("Category Converter to service Test")
    void convertToServiceTest() {
        Assertions.assertEquals(categoryTransactionModelService, categoryConverter.toService(categoryTransactionModel));
    }

}
