package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionDirection;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class TransactionRepository {
    public static final String INSERT = "INSERT into transaction_tbl " +
            "(account_id, amount, direction, timestamp) " +
            "VALUES (?, ?, ?, ?)";
    public static final String INSERT_CATEGORY = "INSERT INTO transaction_categories_tbl " +
            "(category_id, transaction_id) " +
            "VALUES (?, ?)";
    public static final String DELETE = "DELETE FROM transaction_tbl " +
            "WHERE id = ?";
    public static final String DELETE_CATEGORY = "DELETE FROM transaction_categories_tbl " +
            "WHERE transaction_id = ?";
    public static final String SELECT = "SELECT id, account_id, amount, timestamp, direction FROM transaction_tbl " +
            "WHERE id = ?";
    public static final String SELECT_ALL = "SELECT id, account_id, amount, timestamp, direction FROM transaction_tbl";
    public static final String SELECT_CATEGORY = "SELECT id FROM category_tbl AS ct " +
            "INNER JOIN transaction_categories_tbl AS ctt ON ctt.category_id = ct.id " +
            "WHERE transaction_id = ?";
    public static final String UPDATE = "UPDATE transaction_tbl SET " +
            "account_id = ?, amount = ?, direction = ?, timestamp = ? " +
            "WHERE id = ?";
    public static final String FIND_IN_CATEGORY =
            "SELECT t.id, account_id, amount, timestamp, direction FROM transaction_tbl AS t " +
                    "INNER JOIN transaction_categories_tbl AS tc ON tc.transaction_id = t.id WHERE tc.category_id = ?";

    private DBConnectorInterface dbConnector;

    public TransactionRepository() {
        this.dbConnector = new DBConnector();
    }

    public TransactionRepository(DBConnectorInterface dbConnector) {
        this.dbConnector = dbConnector;
    }

    private void modelFieldsCheck(TransactionModel model) {
        if (model.getAccount() == null || model.getTimeStamp() == null || model.getDirection() == null) {
            String hfErrorMsg = "Model fields can't be empty.";
            throw new HomefinanceDaoException(hfErrorMsg);
        }
    }

    public Long save(TransactionModel model) {  // todo возврат через optional, P = 2
        Connection connection = dbConnector.connect();
        Long newTransactionId = null;

        if (model.getId() != null) {
            if (findById(model.getId()) != null) {
                String hfErrorMsg = "Already have entry with same ID.";
                throw new HomefinanceDaoException(hfErrorMsg);
            }
        }

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setLong(1, model.getAccount().getId());
            preparedStatement.setBigDecimal(2, model.getAmount());
            preparedStatement.setString(3, model.getDirection().name());
            preparedStatement.setTimestamp(4, java.sql.Timestamp.valueOf(model.getTimeStamp()));
            preparedStatement.executeUpdate();

            ResultSet queryResult = preparedStatement.getGeneratedKeys();

            if (queryResult.next()) {
                newTransactionId = queryResult.getLong(1);
                model.setId(newTransactionId);
            }

            createCategoryLink(newTransactionId, model.getCategory(), connection, dbConnector);

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL saving transaction to SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newTransactionId;
    }

    private static void createCategoryLink(Long transactionId, List<CategoryTransactionModel> categories, Connection connection, DBConnectorInterface dbConnector) throws SQLException {
        for (CategoryTransactionModel category : categories) {
            if (category.getId() == null) {
                CategoryRepository categoryRepository = new CategoryRepository(dbConnector);
                category.setId(categoryRepository.save(category));
            }

            PreparedStatement preparedStatementInsertCategory = connection.prepareStatement(INSERT_CATEGORY);
            preparedStatementInsertCategory.setLong(1, category.getId());
            preparedStatementInsertCategory.setLong(2, transactionId);
            preparedStatementInsertCategory.executeUpdate();
        }
    }

    public boolean delete(Long id) {
        Connection connection = dbConnector.connect();
        int affectedRows = 0;

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            deleteCategoryLink(id, connection);
            preparedStatement.setLong(1, id);
            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL deleting transaction from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }

    private static void deleteCategoryLink(Long transactionID, Connection connection) throws SQLException {
        PreparedStatement preparedStatementCategory = connection.prepareStatement(DELETE_CATEGORY);

        preparedStatementCategory.setLong(1, transactionID);
        preparedStatementCategory.executeUpdate();
    }

    public TransactionModel findById(Long id) {
        Connection connection = dbConnector.connect();
        AccountRepository accountRepository = new AccountRepository(dbConnector);
        CategoryRepository categoryRepository = new CategoryRepository(dbConnector);

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setLong(1, id);
            ResultSet queryResult = preparedStatement.executeQuery();
            TransactionModel returnValue = new TransactionModel();
            List<CategoryTransactionModel> categoryList = new ArrayList<>();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            if (queryResult.next()) {
                returnValue.setId(queryResult.getLong("id"));
                returnValue.setAccount(accountRepository.findById(queryResult.getLong("account_id")));
                returnValue.setAmount(queryResult.getBigDecimal("amount"));
                returnValue.setDirection(TransactionDirection.valueOf(queryResult.getString("direction")));
                returnValue.setTimeStamp(LocalDateTime.parse(queryResult.getString("timestamp"), formatter));

                PreparedStatement preparedStatementCategory = connection.prepareStatement(SELECT_CATEGORY);
                preparedStatementCategory.setLong(1, id);
                ResultSet queryResultCategory = preparedStatementCategory.executeQuery();

                while (queryResultCategory.next()) {
                    categoryList.add(categoryRepository.findById(queryResultCategory.getLong("id")));
                }

                returnValue.setCategory(categoryList);

            } else {
                return null;
            }

            return returnValue;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            System.out.println("FAIL reading transaction from SQL");
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public List<TransactionModel> findAll() {
        Connection connection = dbConnector.connect();
        AccountRepository accountRepository = new AccountRepository(dbConnector);
        CategoryRepository categoryRepository = new CategoryRepository(dbConnector);
        List<TransactionModel> returnArray = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            ResultSet queryResult = preparedStatement.executeQuery();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            while (queryResult.next()) {
                List<CategoryTransactionModel> categoryList = new ArrayList<>();
                TransactionModel singleValue = new TransactionModel();

                singleValue.setId(queryResult.getLong("id"));
                singleValue.setAccount(accountRepository.findById(queryResult.getLong("account_id")));
                singleValue.setAmount(queryResult.getBigDecimal("amount"));
                singleValue.setDirection(TransactionDirection.valueOf(queryResult.getString("direction")));
                singleValue.setTimeStamp(LocalDateTime.parse(queryResult.getString("timestamp"), formatter));

                PreparedStatement preparedStatementCategory = connection.prepareStatement(SELECT_CATEGORY);
                preparedStatementCategory.setLong(1, singleValue.getId());
                ResultSet queryResultCategory = preparedStatementCategory.executeQuery();

                while (queryResultCategory.next()) {
                    categoryList.add(categoryRepository.findById(queryResultCategory.getLong("id")));
                }

                singleValue.setCategory(categoryList);

                returnArray.add(singleValue);
            }

            return returnArray;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            System.out.println("FAIL reading transaction from SQL");
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public List<TransactionModel> findByCategory(long categoryId) {
        Connection connection = dbConnector.connect();
        AccountRepository accountRepository = new AccountRepository(dbConnector);
        CategoryRepository categoryRepository = new CategoryRepository(dbConnector);
        List<TransactionModel> returnArray = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_IN_CATEGORY)) {
            preparedStatement.setLong(1, categoryId);
            ResultSet queryResult = preparedStatement.executeQuery();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            while (queryResult.next()) {
                List<CategoryTransactionModel> categoryList = new ArrayList<>();
                TransactionModel singleValue = new TransactionModel();

                singleValue.setId(queryResult.getLong("id"));
                singleValue.setAccount(accountRepository.findById(queryResult.getLong("account_id")));
                singleValue.setAmount(queryResult.getBigDecimal("amount"));
                singleValue.setDirection(TransactionDirection.valueOf(queryResult.getString("direction")));
                singleValue.setTimeStamp(LocalDateTime.parse(queryResult.getString("timestamp"), formatter));

                PreparedStatement preparedStatementCategory = connection.prepareStatement(SELECT_CATEGORY);
                preparedStatementCategory.setLong(1, singleValue.getId());
                ResultSet queryResultCategory = preparedStatementCategory.executeQuery();

                while (queryResultCategory.next()) {
                    categoryList.add(categoryRepository.findById(queryResultCategory.getLong("id")));
                }

                singleValue.setCategory(categoryList);

                returnArray.add(singleValue);
            }

            return returnArray;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            System.out.println("FAIL reading transaction from SQL");
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public boolean update(TransactionModel model) {
        int affectedRows = 0;
        Connection connection = dbConnector.connect();

        modelFieldsCheck(model);

        if (model.getAccount() == null) {
            throw new HomefinanceDaoException("No account specified for transaction.");
        } else if (model.getId() == null) {
            throw new HomefinanceDaoException("Transactions with no id not allowed.");
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setLong(1, model.getAccount().getId());
            preparedStatement.setBigDecimal(2, model.getAmount());
            preparedStatement.setString(3, model.getDirection().name());
            preparedStatement.setTimestamp(4, java.sql.Timestamp.valueOf(model.getTimeStamp()));
            preparedStatement.setLong(5, model.getId());
            deleteCategoryLink(model.getId(), connection);
            createCategoryLink(model.getId(), model.getCategory(), connection, dbConnector);
            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL updating transaction in SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }
}
