package ru.geekfactory.homefinance.service.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
//todo найти uder-аннотацию ломбока, которая заменияет все типовые геттеры и сеттеры
// spoiler - аннотация DATA
public class AccountModelService {
    private Long id;
    private String name;
    private BigDecimal amount;
    private CurrencyModelService currency;

    @Override
    public String toString() {
        return "AccountModelService{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", currency=" + currency +
                '}';
    }
}
