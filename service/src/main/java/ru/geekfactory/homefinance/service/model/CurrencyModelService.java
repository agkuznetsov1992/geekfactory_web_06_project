package ru.geekfactory.homefinance.service.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Optional;

@Getter
@Setter
public class CurrencyModelService {
    private Long id;
    private String name;
    private String code;
    private BigDecimal courseTo;
    private CurrencyModelService currencyTo;

    @Override
    public String toString() {
        Optional<CurrencyModelService> optCTM = Optional.ofNullable(this.currencyTo);

        return "CurrencyModelService{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", courseTo=" + courseTo +
                ", currencyTo=" + optCTM.map(CurrencyModelService::getCode).orElse(this.getCode()) +
                '}';
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof CurrencyModelService)) return false;
        final CurrencyModelService other = (CurrencyModelService) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$code = this.getCode();
        final Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) return false;
        final Object this$courseTo = this.getCourseTo();
        final Object other$courseTo = other.getCourseTo();
        if (this$courseTo == null ? other$courseTo != null : !this$courseTo.equals(other$courseTo)) return false;
        final Object this$currencyTo = this.getCurrencyTo();
        final Object other$currencyTo = other.getCurrencyTo();
        if (this$currencyTo == null ? other$currencyTo != null : !((CurrencyModelService) this$currencyTo).getId().equals(((CurrencyModelService) other$currencyTo).getId()))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CurrencyModelService;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $code = this.getCode();
        result = result * PRIME + ($code == null ? 43 : $code.hashCode());
        final Object $courseTo = this.getCourseTo();
        result = result * PRIME + ($courseTo == null ? 43 : $courseTo.hashCode());
        final Object $currencyTo = this.getCurrencyTo();
        result = result * PRIME + ($currencyTo == null ? 43 : $currencyTo.hashCode());
        return result;
    }
}
