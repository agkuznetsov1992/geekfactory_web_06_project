package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.*;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CategoryRepositoryTest {
    public static final String DELETEALL = "DELETE from transaction_categories_tbl;" +
            " DELETE from category_tbl;";
    private DBConnectorInterface dbConnectorH2;
//    unfinished mock
//    DBConnectorInterface dbConnectorMock = mock(DBConnector.class);

    @BeforeEach
    @AfterEach
    void clearTables() {
        dbConnectorH2 = new DBConnectorH2();
        Connection connection = dbConnectorH2.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETEALL)) {
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            String hfErrorMsg = "FAIL truncating H2 category table.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    @Test
    @DisplayName("Save and find operation testing")
    void saveAndFindTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("testH2Category");

        Long newCategoryId = categoryRepository.save(categoryTransactionModel);
        CategoryTransactionModel categoryTransactionModelReaded = categoryRepository.findById(newCategoryId);

        assertEquals(categoryTransactionModel, categoryTransactionModelReaded);
    }

    @Test
    @DisplayName("Save models with same ID testing")
    void saveDuplicateTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("testH2Category");

        categoryRepository.save(categoryTransactionModel);

        assertThrows(HomefinanceDaoException.class, () -> {
            categoryRepository.save(categoryTransactionModel);
        });
    }

    @Test
    @DisplayName("Find all operation testing")
    void findAllTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        List<CategoryTransactionModel> categoryList = new ArrayList<>();
        List<CategoryTransactionModel> categoryListReaded = new ArrayList<>();

        for (int x = 0; x < 10; x++) {
            CategoryTransactionModel singleCategory = new CategoryTransactionModel();
            singleCategory.setName("testH2Category " + x);
            categoryList.add(singleCategory);
        }

        for (CategoryTransactionModel x : categoryList) {
            categoryRepository.save(x);
        }

        categoryListReaded = categoryRepository.findAll();
        Assertions.assertEquals(categoryList, categoryListReaded);
    }

    @Test
    @DisplayName("Save model with empty fields testing")
    void saveEmptyFieldsTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("");

        assertThrows(HomefinanceDaoException.class, () -> {
            categoryRepository.save(categoryTransactionModel);
        });

        categoryTransactionModel.setName(null);

        assertThrows(HomefinanceDaoException.class, () -> {
            categoryRepository.save(categoryTransactionModel);
        });
    }

    @Test
    @DisplayName("Update operation testing")
    void updateTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("testH2Category");

        categoryRepository.save(categoryTransactionModel);
        categoryTransactionModel.setName("testH2CategoryUpdate");
        categoryRepository.update(categoryTransactionModel);

        CategoryTransactionModel categoryTransactionModelReaded = categoryRepository.findById(categoryTransactionModel.getId());

        assertEquals(categoryTransactionModel, categoryTransactionModelReaded);
    }

    @Test
    @DisplayName("Update empty fields testing")
    void updateEmptyTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("testH2Category");

        categoryRepository.save(categoryTransactionModel);
        categoryTransactionModel.setName("");

        assertThrows(HomefinanceDaoException.class, () -> {
            categoryRepository.update(categoryTransactionModel);
        });

        categoryTransactionModel.setName(null);

        assertThrows(HomefinanceDaoException.class, () -> {
            categoryRepository.update(categoryTransactionModel);
        });
    }

    @Test
    @DisplayName("Delete operation testing")
    void deleteTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("testH2Category");

        categoryRepository.save(categoryTransactionModel);
        categoryRepository.delete(categoryTransactionModel.getId());

        CategoryTransactionModel categoryTransactionModelReaded = categoryRepository.findById(categoryTransactionModel.getId());

        assertNull(categoryTransactionModelReaded);
    }

    @Test
    @DisplayName("Delete non-existing row operation testing")
    void deleteNETest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setName("testH2Category");
        categoryRepository.save(categoryTransactionModel);

        assertTrue(categoryRepository.delete(categoryTransactionModel.getId()));
        assertFalse(categoryRepository.delete(categoryTransactionModel.getId()));
    }
}
