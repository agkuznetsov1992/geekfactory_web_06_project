package ru.geekfactory.homefinance.service.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.model.AccountModelService;
import ru.geekfactory.homefinance.service.model.CurrencyModelService;

import java.math.BigDecimal;

public class AccountConverterTest {
    AccountModel accountModel = new AccountModel();
    AccountModelService accountModelService = new AccountModelService();
    CurrencyModel currencyModel = new CurrencyModel();
    CurrencyModelService currencyModelService = new CurrencyModelService();
    AccountConverter accountConverter = new AccountConverter();

    @BeforeEach
    void prepareModels() {
        currencyModel.setId(1L);
        currencyModel.setName("test currency");
        currencyModel.setCode("TC");
        currencyModel.setCourseTo(BigDecimal.ONE);
        currencyModel.setCurrencyTo(currencyModel);

        currencyModelService.setId(1L);
        currencyModelService.setName("test currency");
        currencyModelService.setCode("TC");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);

        accountModel.setId(1L);
        accountModel.setName("Test Account");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(new BigDecimal(3000));

        accountModelService.setId(1L);
        accountModelService.setName("Test Account");
        accountModelService.setCurrency(currencyModelService);
        accountModelService.setAmount(new BigDecimal(3000));

    }

    @Test
    @DisplayName("Account Converter to dao Test")
    void convertToDaoTest() {
        //todo разбить ассерты на два разных теста
        Assertions.assertEquals(accountModel.toString(), accountConverter.toDao(accountModelService).toString());
    }

    @Test
    @DisplayName("Account Converter to service Test")
    void convertToServiceTest(){
        Assertions.assertEquals(accountModelService.toString(), accountConverter.toService(accountModel).toString());
    }
}
