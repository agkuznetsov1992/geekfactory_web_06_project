package ru.geekfactory.homefinance.service.converter;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.model.CurrencyModelService;

public class CurrencyConverter {
    public CurrencyModel toDao(CurrencyModelService currencyModelService) {
        CurrencyModel currencyModel = new CurrencyModel();

        currencyModel.setId(currencyModelService.getId());
        currencyModel.setName(currencyModelService.getName());
        currencyModel.setCode(currencyModelService.getCode());
        currencyModel.setCourseTo(currencyModelService.getCourseTo());

        if(currencyModelService.equals(currencyModelService.getCurrencyTo())){
            currencyModel.setCurrencyTo(currencyModel);
        } else {
            currencyModel.setCurrencyTo(this.toDao(currencyModelService.getCurrencyTo()));
        }

        return currencyModel;
    }

    public CurrencyModelService toService(CurrencyModel currencyModel){
        CurrencyModelService currencyModelService = new CurrencyModelService();

        currencyModelService.setId(currencyModel.getId());
        currencyModelService.setName(currencyModel.getName());
        currencyModelService.setCode(currencyModel.getCode());
        currencyModelService.setCourseTo(currencyModel.getCourseTo());

        if(currencyModel.getCurrencyTo().getId() == currencyModel.getId()){
            currencyModelService.setCurrencyTo(currencyModelService);
        } else {
            currencyModelService.setCurrencyTo(this.toService(currencyModel.getCurrencyTo()));
        }

        return currencyModelService;
    }
}
