package ru.geekfactory.homefinance.dao.repository;

public class HomefinanceDaoException extends RuntimeException {
    public HomefinanceDaoException(String message) {
        super(message);
    }

    public HomefinanceDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public HomefinanceDaoException(Throwable cause) {
        super(cause);
    }
}
