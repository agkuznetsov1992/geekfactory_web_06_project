package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;
import ru.geekfactory.homefinance.service.converter.AccountConverter;
import ru.geekfactory.homefinance.service.model.AccountModelService;
import ru.geekfactory.homefinance.service.model.CurrencyModelService;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceTest {
    private static final MathContext BD_CONTEXT = new MathContext(2, RoundingMode.HALF_UP);
    AccountRepository accountRepository = mock(AccountRepository.class);
    AccountModel accountModel = new AccountModel();
    AccountModelService accountModelService = new AccountModelService();
    AccountConverter accountConverter = new AccountConverter();
    AccountService accountService = new AccountService(accountRepository);
    CurrencyModelService currencyModelService = new CurrencyModelService();
    CurrencyModel currencyModel = new CurrencyModel();

    @BeforeEach
    private void prepareModels() {
        currencyModelService.setId(0L);
        currencyModelService.setName("Ruble");
        currencyModelService.setCode("RUR");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);
        accountModelService.setId(0L);
        accountModelService.setName("test acct");
        accountModelService.setAmount(new BigDecimal(4500, BD_CONTEXT));
        accountModelService.setCurrency(currencyModelService);
    }

    @Test
    @DisplayName("Account service save test")
    public void saveTest() {
        long expected = accountModelService.getId();

        when(accountRepository.save(accountConverter.toDao(accountModelService))).thenReturn(accountModelService.getId());
        Assertions.assertEquals(expected, accountService.save(accountModelService));
    }

    @Test
    @DisplayName("Account service findall test")
    public void findAllTest() {
        int i = 0;
        List<AccountModel> accountModelList = new ArrayList<>();
        List<AccountModelService> expected = new ArrayList<>();
        currencyModel.setId(0L);
        currencyModel.setName("Ruble");
        currencyModel.setCode("RUR");
        currencyModel.setCourseTo(BigDecimal.ONE);
        currencyModel.setCurrencyTo(currencyModel);
        when(accountRepository.findAll()).thenReturn(accountModelList);

        while (i < 3) {
            accountModel = new AccountModel();
            accountModel.setId((long) i);
            accountModel.setName("test acct");
            accountModel.setAmount(new BigDecimal(4500, BD_CONTEXT));
            accountModel.setCurrency(currencyModel);
            accountModelList.add(accountModel);
            i++;
        }

        for (AccountModel x : accountModelList) {
            expected.add(accountConverter.toService(x));
        }

        Assertions.assertEquals(expected, accountService.getTotalAccounts());
    }

    @Test
    @DisplayName("Account service update test")
    public void updateTest() {
        when(accountRepository.update(accountConverter.toDao(accountModelService))).thenReturn(true).thenReturn(false);

        Assertions.assertTrue(accountService.update(accountModelService));
        Assertions.assertFalse(accountService.update(accountModelService));
    }

    @Test
    @DisplayName("Account service update test")
    public void deleteTest() {
        when(accountRepository.delete(0L)).thenReturn(true).thenReturn(false);

        Assertions.assertTrue(accountService.delete(0L));
        Assertions.assertFalse(accountService.delete(0L));
    }
}
