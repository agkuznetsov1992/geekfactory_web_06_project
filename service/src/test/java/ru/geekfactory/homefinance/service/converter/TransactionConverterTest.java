package ru.geekfactory.homefinance.service.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.service.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

public class TransactionConverterTest {
    AccountModel accountModel = new AccountModel();
    AccountModelService accountModelService = new AccountModelService();
    CurrencyModel currencyModel = new CurrencyModel();
    CurrencyModelService currencyModelService = new CurrencyModelService();
    CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
    CategoryTransactionModelService categoryTransactionModelService = new CategoryTransactionModelService();
    TransactionModel transactionModel = new TransactionModel();
    TransactionModelService transactionModelService = new TransactionModelService();
    TransactionConverter transactionConverter = new TransactionConverter();

    @BeforeEach
    void prepareModels() {
        String date = "2017-03-08T12:30:54";
        LocalDateTime tstamp = LocalDateTime.parse(date);

        categoryTransactionModel.setId(1L);
        categoryTransactionModel.setName("test Category");

        categoryTransactionModelService.setId(1L);
        categoryTransactionModelService.setName("test Category");

        currencyModel.setId(1L);
        currencyModel.setName("test currency");
        currencyModel.setCode("TC");
        currencyModel.setCourseTo(BigDecimal.ONE);
        currencyModel.setCurrencyTo(currencyModel);

        currencyModelService.setId(1L);
        currencyModelService.setName("test currency");
        currencyModelService.setCode("TC");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);

        accountModel.setId(1L);
        accountModel.setName("Test Account");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(new BigDecimal(3000));

        accountModelService.setId(1L);
        accountModelService.setName("Test Account");
        accountModelService.setCurrency(currencyModelService);
        accountModelService.setAmount(new BigDecimal(3000));

        transactionModel.setId(1L);
        transactionModel.setAmount(new BigDecimal(3000));
        transactionModel.setDirection(TransactionDirection.RECEIPT);
        transactionModel.setAccount(accountModel);
        transactionModel.setCategory(Arrays.asList(categoryTransactionModel));
        transactionModel.setTimeStamp(tstamp);

        transactionModelService.setId(1L);
        transactionModelService.setAmount(new BigDecimal(3000));
        transactionModelService.setDirection(TransactionDirectionService.RECEIPT);
        transactionModelService.setAccount(accountModelService);
        transactionModelService.setCategory(Arrays.asList(categoryTransactionModelService));
        transactionModelService.setTimeStamp(tstamp);
    }

    @Test
    @DisplayName("Transaction Converter to dao Test")
    void convertToDaoTest() {
        Assertions.assertEquals(transactionModel.toString(), transactionConverter.toDao(transactionModelService).toString());
    }

    @Test
    @DisplayName("Transaction Converter to service Test")
    void convertToServiceTest() {
        Assertions.assertEquals(transactionModelService.toString(), transactionConverter.toService(transactionModel).toString());
    }
}
