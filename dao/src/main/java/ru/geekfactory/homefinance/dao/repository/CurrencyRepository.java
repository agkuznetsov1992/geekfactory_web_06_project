package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CurrencyRepository {
    public static final String INSERT = "INSERT into currency_tbl " +
            "(name, code, course_to, currency_to_id) " +
            "VALUES (?, ?, ?, ?)";
    public static final String SELECT = "SELECT id, name, code, course_to, currency_to_id from currency_tbl " +
            "WHERE id = ?";
    public static final String SELECTALL = "SELECT id, name, code, course_to, currency_to_id from currency_tbl";
    public static final String UPDATE = "UPDATE currency_tbl SET " +
            "name = ?, code = ?, course_to = ?, currency_to_id = ? " +
            "WHERE id = ?";
    public static final String DELETE = "DELETE FROM currency_tbl " +
            "WHERE id = ?";
    private DBConnectorInterface dbConnector;

    public CurrencyRepository() {
        this.dbConnector = new DBConnector();
    }

    public CurrencyRepository(DBConnectorInterface dbConnector) {
        this.dbConnector = dbConnector;
    }

    private void modelFieldsCheck(CurrencyModel model) {
        if (model.getName() == null || model.getCode() == null || model.getCourseTo() == null || model.getName().equals("") || model.getCode().equals("")) {
            String hfErrorMsg = "Model fields can't be empty.";
            throw new HomefinanceDaoException(hfErrorMsg);
        }
    }

    public Long save(CurrencyModel model) {
        Connection connection = dbConnector.connect();
        Long newCurrencyId = null;

        if (model.getId() != null) {
            if (findById(model.getId()) != null) {
                String hfErrorMsg = "Already have entry with same ID.";
                throw new HomefinanceDaoException(hfErrorMsg);
            }
        }

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getCode());
            preparedStatement.setBigDecimal(3, model.getCourseTo());

            if (model.getCurrencyTo() != null && model.getCurrencyTo().getId() != model.getId()) {
                preparedStatement.setLong(4, model.getCurrencyTo().getId());
            } else {
                preparedStatement.setNull(4, Types.INTEGER);
            }

            preparedStatement.executeUpdate();

            ResultSet queryResult = preparedStatement.getGeneratedKeys();

            if (queryResult.next()) {
                newCurrencyId = queryResult.getLong(1);
                model.setId(newCurrencyId);
            }

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL saving currency to SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newCurrencyId;
    }

    public boolean update(CurrencyModel model) {
        Connection connection = dbConnector.connect();
        int affectedRows = 0;

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getCode());
            preparedStatement.setBigDecimal(3, model.getCourseTo());

            if (model.getCurrencyTo() != null) {
                preparedStatement.setLong(4, model.getCurrencyTo().getId());
            } else {
                preparedStatement.setNull(4, Types.INTEGER);
            }

            preparedStatement.setLong(5, model.getId());

            affectedRows = preparedStatement.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL updating currency in SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean delete(Long id) {
        Connection connection = dbConnector.connect();
        int affectedRows = 0;

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setLong(1, id);

            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL deleting currency from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }

    public CurrencyModel findById(Long id) {
        Connection connection = dbConnector.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setLong(1, id);
            ResultSet queryResult = preparedStatement.executeQuery();
            CurrencyModel returnValue = new CurrencyModel();

            if (queryResult.next()) {
                returnValue.setId(queryResult.getLong("id"));
                returnValue.setName(queryResult.getString("name"));
                returnValue.setCode(queryResult.getString("code"));
                returnValue.setCourseTo(queryResult.getBigDecimal("course_to"));

                //не уверен про 0 и null в таблице, надо еще раз проверить как ведет себя SQL при записе NULL в INT
                if (queryResult.getLong("currency_to_id") != 0) {
                    returnValue.setCurrencyTo(this.findById(queryResult.getLong("currency_to_id")));
                } else if (queryResult.getLong("currency_to_id") == queryResult.getLong("id")) {
                    returnValue.setCurrencyTo(returnValue);
                } else {
                    returnValue.setCurrencyTo(null);
                }
            } else {
                return null;
            }

            return returnValue;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

            String hfErrorMsg = "FAIL reading currency from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    public List<CurrencyModel> findAll() {
        Connection connection = dbConnector.connect();
        List<CurrencyModel> returnArray = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECTALL)) {
            ResultSet queryResult = preparedStatement.executeQuery();

            while (queryResult.next()) {
                CurrencyModel singleValue = new CurrencyModel();
                singleValue.setId(queryResult.getLong("id"));
                singleValue.setName(queryResult.getString("name"));
                singleValue.setCode(queryResult.getString("code"));
                singleValue.setCourseTo(queryResult.getBigDecimal("course_to"));

                //не уверен про 0 и null в таблице, надо еще раз проверить как ведет себя SQL при записе NULL в INT
                if (queryResult.getLong("currency_to_id") != 0) {
                    singleValue.setCurrencyTo(this.findById(queryResult.getLong("currency_to_id")));
                } else if (queryResult.getLong("currency_to_id") == queryResult.getLong("id")) {
                    singleValue.setCurrencyTo(singleValue);
                } else {
                    singleValue.setCurrencyTo(null);
                }

                returnArray.add(singleValue);
            }

            return returnArray;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

            String hfErrorMsg = "FAIL reading currency from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }


    }
}
