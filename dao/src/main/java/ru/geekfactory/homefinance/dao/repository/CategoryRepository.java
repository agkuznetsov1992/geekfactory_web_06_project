package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryRepository {
    public static final String INSERT = "INSERT into category_tbl " +
            "(name) " +
            "VALUES (?)";
    public static final String SELECT = "SELECT id, name from category_tbl " +
            "WHERE id = ?";
    public static final String SELECTALL = "SELECT * FROM category_tbl";
    public static final String UPDATE = "UPDATE category_tbl SET " +
            "name = ? " +
            "WHERE id = ?";
    public static final String DELETE = "DELETE FROM category_tbl " +
            "WHERE id = ?";
    private DBConnectorInterface dbConnector;

    public CategoryRepository() {
        this.dbConnector = new DBConnector();
    }

    public CategoryRepository(DBConnectorInterface dbConnector) {
        this.dbConnector = dbConnector;
    }

    private void modelFieldsCheck(CategoryTransactionModel model) {
        if (model.getName() == null || model.getName().equals("")) {
            String hfErrorMsg = "Model fields can't be empty.";
            throw new HomefinanceDaoException(hfErrorMsg);
        }
    }

    public Long save(CategoryTransactionModel model) {
        Connection connection = dbConnector.connect();
        Long newEntryIndex;

        if (model.getId() != null) {
            if (findById(model.getId()) != null) {
                String hfErrorMsg = "Already have entry with same ID.";
                throw new HomefinanceDaoException(hfErrorMsg);
            }
        }

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, model.getName());

            preparedStatement.executeUpdate();
            ResultSet newId = preparedStatement.getGeneratedKeys();

            if (newId.next()) {
                newEntryIndex = newId.getLong(1);
                model.setId(newEntryIndex);
            } else {
                throw new HomefinanceDaoException("Can't get new SQL entry ID");
            }

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL saving category to SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newEntryIndex;
    }

    public boolean update(CategoryTransactionModel model) {
        int affectedRows = 0;
        Connection connection = dbConnector.connect();

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setLong(2, model.getId());

            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL updating category in SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        if(affectedRows == 1){
            return true;
        } else {
            return false;
        }
    }

    public boolean delete(Long id) {
        Connection connection = dbConnector.connect();
        int affectedRows = 0;

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setLong(1, id);

            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL deleting category from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }

    public CategoryTransactionModel findById(Long id) {
        Connection connection = dbConnector.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setLong(1, id);
            ResultSet queryResult = preparedStatement.executeQuery();
            CategoryTransactionModel returnValue = new CategoryTransactionModel();

            if (queryResult.next()) {
                returnValue.setId(queryResult.getLong("id"));
                returnValue.setName(queryResult.getString("name"));
            } else {
                return null;
            }

            return returnValue;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL reading category from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    public List<CategoryTransactionModel> findAll() {
        List<CategoryTransactionModel> returnArray = new ArrayList<>();
        Connection connection = dbConnector.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECTALL)) {
            ResultSet queryResult = preparedStatement.executeQuery();

            while (queryResult.next()) {
                CategoryTransactionModel singleValue = new CategoryTransactionModel();

                singleValue.setId(queryResult.getLong("id"));
                singleValue.setName(queryResult.getString("name"));
                returnArray.add(singleValue);
            }

            return returnArray;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL reading categories from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }
}
