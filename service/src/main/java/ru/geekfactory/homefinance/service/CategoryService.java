package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.repository.CategoryRepository;
import ru.geekfactory.homefinance.service.converter.CategoryConverter;
import ru.geekfactory.homefinance.service.model.CategoryTransactionModelService;

import java.util.ArrayList;
import java.util.List;

public class CategoryService {
    CategoryRepository categoryRepository;
    CategoryConverter categoryConverter = new CategoryConverter();

    public CategoryService() {
        this.categoryRepository = new CategoryRepository();
    }

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    //todo: test method, need to be destroyed later
    public CategoryTransactionModel categoryTestService(Long id) {
        CategoryTransactionModel retVal = new CategoryTransactionModel();
        retVal = categoryRepository.findById(id);

        return retVal;
    }

    public Long save (CategoryTransactionModelService categoryTransactionModelService){
        CategoryTransactionModel convertedModel = categoryConverter.toDao(categoryTransactionModelService);

        return categoryRepository.save(convertedModel);
    }

    //todo конвертировать в service модели
    public List<CategoryTransactionModel> getTotalCategories() {
        List<CategoryTransactionModel> returnArray = new ArrayList<>();
        returnArray = categoryRepository.findAll();

        return returnArray;
    }

    public boolean update(CategoryTransactionModelService categoryTransactionModelService) {
        CategoryTransactionModel convertedModel = categoryConverter.toDao(categoryTransactionModelService);

        return categoryRepository.update(convertedModel);
    }

    public boolean delete(long id) {
        return categoryRepository.delete(id);
    }
}
