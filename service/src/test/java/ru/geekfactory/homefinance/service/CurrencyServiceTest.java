package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.service.converter.CurrencyConverter;
import ru.geekfactory.homefinance.service.model.CurrencyModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CurrencyServiceTest {
    CurrencyRepository currencyRepository = mock(CurrencyRepository.class);
    CurrencyModel currencyModel = new CurrencyModel();
    CurrencyModelService currencyModelService = new CurrencyModelService();
    CurrencyConverter currencyConverter = new CurrencyConverter();
    CurrencyService currencyService = new CurrencyService(currencyRepository);

    @Test
    @DisplayName("Currency service save test")
    public void saveTest() {
        currencyModelService.setId(0L);
        currencyModelService.setName("Ruble");
        currencyModelService.setCode("RUR");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);

        long expected = currencyModelService.getId();
        when(currencyRepository.save(currencyConverter.toDao(currencyModelService))).
                thenReturn(currencyModelService.getId());

        Assertions.assertEquals(expected, currencyService.save(currencyModelService));
    }

    @Test
    @DisplayName("Category service findall test")
    public void findAllTest() {
        List<CurrencyModel> currencyModelList = new ArrayList<>();
        List<CurrencyModelService> expected = new ArrayList<>();
        int i = 0;

        while (i < 3) {
            currencyModel = new CurrencyModel();
            currencyModel.setId((long) i);
            currencyModel.setName("Ruble");
            currencyModel.setCode("RUR");
            currencyModel.setCourseTo(BigDecimal.ONE);
            currencyModel.setCurrencyTo(currencyModel);

            currencyModelList.add(currencyModel);
            i++;
        }

        for (CurrencyModel x : currencyModelList) {
            expected.add(currencyConverter.toService(x));
        }

        when(currencyRepository.findAll()).thenReturn(currencyModelList);
        Assertions.assertEquals(expected, currencyService.getTotalCurrencies());
    }

    @Test
    @DisplayName("Currency service update test")
    public void updateTest() {
        currencyModelService.setId(0L);
        currencyModelService.setName("Ruble");
        currencyModelService.setCode("RUR");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);

        when(currencyRepository.update(currencyConverter.toDao(currencyModelService))).thenReturn(true).thenReturn(false);

        Assertions.assertTrue(currencyService.update(currencyModelService));
        Assertions.assertFalse(currencyService.update(currencyModelService));
    }

    @Test
    @DisplayName("Currency service delete test")
    public void deleteTest() {
        when(currencyRepository.delete(0L)).thenReturn(true).thenReturn(false);

        Assertions.assertTrue(currencyService.delete(0L));
        Assertions.assertFalse(currencyService.delete(0L));
    }
}
