package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.repository.CategoryRepository;
import ru.geekfactory.homefinance.service.converter.CategoryConverter;
import ru.geekfactory.homefinance.service.model.CategoryTransactionModelService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryServiceTest {
    CategoryRepository categoryRepository = mock(CategoryRepository.class);
    CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
    CategoryTransactionModelService categoryTransactionModelService = new CategoryTransactionModelService();
    CategoryConverter categoryConverter = new CategoryConverter();

    @Test
    @DisplayName("Category service save test")
    public void saveTest() {
        categoryTransactionModelService.setName("1s");
        categoryTransactionModelService.setId(0L);
        long expected = categoryTransactionModelService.getId();
        CategoryService categoryService = new CategoryService(categoryRepository);

        when(categoryRepository.save(categoryConverter.toDao(categoryTransactionModelService))).
                thenReturn(categoryTransactionModelService.getId());
        Assertions.assertEquals(expected, categoryService.save(categoryTransactionModelService).longValue());
    }

    @Test
    @DisplayName("Category service findall test")
    public void findAllTest() {
        List<CategoryTransactionModel> expected = new ArrayList<>();
        CategoryService categoryService = new CategoryService(categoryRepository);
        int i = 0;

        while (i < 3) {
            categoryTransactionModel = new CategoryTransactionModel();
            categoryTransactionModel.setName("test " + i);
            categoryTransactionModel.setId((long) i);
            expected.add(categoryTransactionModel);
            i++;
        }

        when(categoryRepository.findAll()).thenReturn(expected);
        Assertions.assertEquals(expected, categoryService.getTotalCategories());
    }

    @Test
    @DisplayName("Category service update test")
    public void updateTest() {
        categoryTransactionModelService.setName("1s");
        categoryTransactionModelService.setId(0L);
        CategoryService categoryService = new CategoryService(categoryRepository);

        when(categoryRepository.update(categoryConverter.toDao(categoryTransactionModelService))).thenReturn(true).thenReturn(false);
        Assertions.assertTrue(categoryService.update(categoryTransactionModelService));
        Assertions.assertFalse(categoryService.update(categoryTransactionModelService));
    }

    @Test
    @DisplayName("Category service delete test")
    public void deleteTest() {
        CategoryService categoryService = new CategoryService(categoryRepository);

        when(categoryRepository.delete(0L)).thenReturn(true);
        when(categoryRepository.delete(3L)).thenReturn(false);
        Assertions.assertTrue(categoryService.delete(0L));
        Assertions.assertFalse(categoryService.delete(3L));
    }
}
