package ru.geekfactory.homefinance.service.converter;

import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.service.model.AccountModelService;

public class AccountConverter {
    CurrencyConverter currencyConverter = new CurrencyConverter();

    //todo вынести рекпозитории в поля класса
    public AccountModel toDao(AccountModelService accountModelService) {
        AccountModel accountModel = new AccountModel();

        accountModel.setId(accountModelService.getId());
        accountModel.setName(accountModelService.getName());
        accountModel.setCurrency(currencyConverter.toDao(accountModelService.getCurrency()));
        accountModel.setAmount(accountModelService.getAmount());

        return accountModel;
    }

    public AccountModelService toService(AccountModel accountModel) {
        AccountModelService accountModelService = new AccountModelService();

        accountModelService.setId(accountModel.getId());
        accountModelService.setName(accountModel.getName());
        accountModelService.setCurrency(currencyConverter.toService(accountModel.getCurrency()));
        accountModelService.setAmount(accountModel.getAmount());

        return accountModelService;
    }

}
