package ru.geekfactory.homefinance.service.converter;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionDirection;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.service.model.CategoryTransactionModelService;
import ru.geekfactory.homefinance.service.model.TransactionDirectionService;
import ru.geekfactory.homefinance.service.model.TransactionModelService;

import java.util.ArrayList;
import java.util.List;

public class TransactionConverter {
    public TransactionModel toDao(TransactionModelService transactionModelService){
        TransactionModel transactionModel = new TransactionModel();
        CurrencyConverter currencyConverter = new CurrencyConverter();
        AccountConverter accountConverter = new AccountConverter();
        CategoryConverter categoryConverter = new CategoryConverter();
        List<CategoryTransactionModel> categoryList = new ArrayList();

        for(CategoryTransactionModelService x : transactionModelService.getCategory()){
            categoryList.add(categoryConverter.toDao(x));
        }

        transactionModel.setId(transactionModelService.getId());
        transactionModel.setTimeStamp(transactionModelService.getTimeStamp());
        transactionModel.setCategory(categoryList);
        transactionModel.setAccount(accountConverter.toDao(transactionModelService.getAccount()));
        transactionModel.setDirection(TransactionDirection.valueOf(transactionModelService.getDirection().toString()));
        transactionModel.setAmount(transactionModelService.getAmount());

        return transactionModel;
    }

    public TransactionModelService toService(TransactionModel transactionModel){
        TransactionModelService transactionModelService = new TransactionModelService();
        CurrencyConverter currencyConverter = new CurrencyConverter();
        AccountConverter accountConverter = new AccountConverter();
        CategoryConverter categoryConverter = new CategoryConverter();
        List<CategoryTransactionModelService> categoryList = new ArrayList();

        for(CategoryTransactionModel x : transactionModel.getCategory()){
            categoryList.add(categoryConverter.toService(x));
        }

        transactionModelService.setId(transactionModel.getId());
        transactionModelService.setTimeStamp(transactionModel.getTimeStamp());
        transactionModelService.setCategory(categoryList);
        transactionModelService.setAccount(accountConverter.toService(transactionModel.getAccount()));
        transactionModelService.setDirection(TransactionDirectionService.valueOf(transactionModel.getDirection().toString()));
        transactionModelService.setAmount(transactionModel.getAmount());

        return transactionModelService;
    }

}
