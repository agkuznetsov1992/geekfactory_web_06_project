package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.*;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CurrencyRepositoryTest {

    public static final String DELETEALL = "DELETE from currency_tbl;";
    private DBConnectorInterface dbConnectorH2;

    @BeforeEach
    @AfterEach
    void clearTables() {
        dbConnectorH2 = new DBConnectorH2();
        Connection connection = dbConnectorH2.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETEALL)) {
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            String hfErrorMsg = "FAIL truncating H2 category table.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    @Test
    @DisplayName("Save and find operation testing")
    void saveAndFindTest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        Long newCurrencyId = currencyRepository.save(currencyModel);

        CurrencyModel currencyModelReaded = currencyRepository.findById(newCurrencyId);

        Assertions.assertEquals(currencyModel, currencyModelReaded);
    }

    @Test
    @DisplayName("Save models with same ID testing")
    void saveDuplicateTest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        currencyRepository.save(currencyModel);
        assertThrows(HomefinanceDaoException.class, () -> {
            currencyRepository.save(currencyModel);
        });
    }

    @Test
    @DisplayName("Find all operation testing")
    void findAllTest() {
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);
        CurrencyModel currencyModel = new CurrencyModel();
        List<CurrencyModel> currencyModelList = new ArrayList<>();
        List<CurrencyModel> currencyModelListReaded = new ArrayList<>();

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        currencyRepository.save(currencyModel);

        for (int x = 0; x < 10; x++) {
            CurrencyModel singleCurrency = new CurrencyModel();
            singleCurrency.setCode("H2 " + x);
            singleCurrency.setName("testH2Currency " + x);
            singleCurrency.setCourseTo(BigDecimal.valueOf(1.01));
            singleCurrency.setCurrencyTo(currencyModel);
            currencyModelList.add(singleCurrency);
        }

        for(CurrencyModel x : currencyModelList){
            currencyRepository.save(x);
        }

        currencyModelList.add(0,currencyModel);
        currencyModelListReaded = currencyRepository.findAll();
        Assertions.assertEquals(currencyModelList,currencyModelListReaded);
    }

    @Test
    @DisplayName("Save model with empty fields testing")
    void saveEmptyFieldsTest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        currencyModel.setCode("");

        assertThrows(HomefinanceDaoException.class, () -> {
            currencyRepository.save(currencyModel);
        });

        currencyModel.setCode("RUR");
        currencyModel.setName("");

        assertThrows(HomefinanceDaoException.class, () -> {
            currencyRepository.save(currencyModel);
        });

        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(null);

        assertThrows(HomefinanceDaoException.class, () -> {
            currencyRepository.save(currencyModel);
        });
    }

    @Test
    @DisplayName("Update operation testing")
    void updateTest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        Long newCurrencyId = currencyRepository.save(currencyModel);

        currencyModel.setName("US Dollar");
        currencyModel.setCode("USD");
        currencyRepository.update(currencyModel);

        CurrencyModel currencyModelReaded = currencyRepository.findById(newCurrencyId);

        assertEquals(currencyModel, currencyModelReaded);
    }

    @Test
    @DisplayName("Update empty fields testing")
    void updateEmptyTest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        Long newCurrencyId = currencyRepository.save(currencyModel);

        currencyModel.setName("US Dollar");
        currencyModel.setCode("");

        assertThrows(HomefinanceDaoException.class, () -> {
            currencyRepository.update(currencyModel);
        });
    }

    @Test
    @DisplayName("Delete operation testing")
    void deleteTest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        Long newCurrencyId = currencyRepository.save(currencyModel);

        currencyRepository.delete(newCurrencyId);

        CurrencyModel currencyModelReaded = currencyRepository.findById(newCurrencyId);
        assertNull(currencyModelReaded);
    }

    @Test
    @DisplayName("Delete non-existing row operation testing")
    void deleteNETest() {
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);

        currencyRepository.save(currencyModel);

        assertTrue(currencyRepository.delete(currencyModel.getId()));
        assertFalse(currencyRepository.delete(currencyModel.getId()));
    }
}
