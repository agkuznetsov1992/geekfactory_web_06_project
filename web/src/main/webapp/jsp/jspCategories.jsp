<%--
  Created by IntelliJ IDEA.
  User: Venom
  Date: 20.04.2019
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>JSP Categories</title>
    <link href="<c:url value="/resources/bootstrap.css"/>" rel="stylesheet">
</head>
<body>
<h1>hello, this is cat page</h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Operations</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="categoryList" items="${categoryList}">
        <tr>
            <td>${categoryList.id}</td>
            <td>${categoryList.name}</td>
            <td>
                <%--todo в action указываем url, а не сервлет--%>
                <form action="${pageContext.request.contextPath}/../../java/ru/geekfactory/homefinance/web/servlet/HelloServlet.java" method="post">
                    <button type="button" class="btn btn-danger" name="delete" value="${categoryList.id}">Delete</button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
