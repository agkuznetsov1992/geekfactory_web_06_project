package ru.geekfactory.homefinance.service.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.model.CurrencyModelService;

import java.math.BigDecimal;

public class CurrencyConverterTest {
    CurrencyModel currencyModel = new CurrencyModel();
    CurrencyModelService currencyModelService = new CurrencyModelService();
    CurrencyConverter currencyConverter = new CurrencyConverter();

    @BeforeEach
    void prepareModels() {
        currencyModel.setId(1L);
        currencyModel.setName("test currency");
        currencyModel.setCode("TC");
        currencyModel.setCourseTo(BigDecimal.ONE);
        currencyModel.setCurrencyTo(currencyModel);

        currencyModelService.setId(1L);
        currencyModelService.setName("test currency");
        currencyModelService.setCode("TC");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);
    }

    @Test
    @DisplayName("Currency Converter to dao Test")
    void convertToDaoTest() {
        Assertions.assertEquals(currencyModel.toString(), currencyConverter.toDao(currencyModelService).toString());
    }

    @Test
    @DisplayName("Currency Converter to service Test")
    void convertToServiceTest() {
        Assertions.assertEquals(currencyModelService.toString(), currencyConverter.toService(currencyModel).toString());
    }
}
