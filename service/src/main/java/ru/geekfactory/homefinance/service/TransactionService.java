package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.repository.DBConnector;
import ru.geekfactory.homefinance.dao.repository.TransactionRepository;
import ru.geekfactory.homefinance.service.converter.TransactionConverter;
import ru.geekfactory.homefinance.service.model.TransactionModelService;

import java.util.ArrayList;
import java.util.List;

public class TransactionService {
    TransactionRepository transactionRepository;
    TransactionConverter transactionConverter = new TransactionConverter();

    public TransactionService() {
        this.transactionRepository = new TransactionRepository();
    }

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public long save(TransactionModelService transactionModelService) {
        TransactionModel convertedModel = transactionConverter.toDao(transactionModelService);

        return transactionRepository.save(convertedModel);
    }

    public List<TransactionModelService> getTotalTransaction() {
        List<TransactionModelService> returnArray = new ArrayList<>();
        List<TransactionModel> transactionModelList = transactionRepository.findAll();

        for (TransactionModel x : transactionModelList) {
            returnArray.add(transactionConverter.toService(x));
        }

        return returnArray;
    }

    public List<TransactionModelService> getTransactionsByCategory(long categoryId) {
        List<TransactionModelService> returnArray = new ArrayList<>();
        List<TransactionModel> transactionModelList = transactionRepository.findByCategory(categoryId);

        for (TransactionModel x : transactionModelList) {
            returnArray.add(transactionConverter.toService(x));
        }

        return returnArray;
    }

    public boolean update(TransactionModelService transactionModelService) {
        TransactionModel convertedModel = transactionConverter.toDao(transactionModelService);

        return transactionRepository.update(convertedModel);
    }

    public List<TransactionModel> getTotalTransactionOld() {
        List<TransactionModel> returnArray = new ArrayList<>();
        DBConnector dbConnector = new DBConnector();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnector);
        returnArray = transactionRepository.findAll();

        return returnArray;
    }

    public boolean delete(long id) {
        return transactionRepository.delete(id);
    }
}
