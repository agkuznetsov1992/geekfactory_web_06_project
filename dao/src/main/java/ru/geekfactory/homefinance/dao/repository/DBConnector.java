package ru.geekfactory.homefinance.dao.repository;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DBConnector implements DBConnectorInterface {
    private boolean isH2 = false;

    public DBConnector() {
    }

    public DBConnector(boolean isH2) {
        this.isH2 = isH2;
    }

    public Connection connect() {
        String propUrl = null;
        String propUser = null;
        String propPasswd = null;
        Properties prop = new Properties();

        try {
            InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("sql_connect.properties");
            prop.load(input);

            propUser = prop.getProperty("DB_USER");
            propPasswd = prop.getProperty("DB_PASSWORD");
            propUrl = prop.getProperty("DB_URL");

            if(propUrl == null || propUser == null || propPasswd == null){
                throw new HomefinanceDaoException("cant read DB credentials from property file");
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(propUrl, propUser, propPasswd);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException | ClassNotFoundException e) {
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public Connection connect(String dbUrl) {
        String propUser = null;
        String propPasswd = null;
        Properties prop = new Properties();

        try {
//            File propFile = new File("dao/src/main/resources/sql_connect.properties");
//            InputStream input = new FileInputStream(propFile);
            InputStream input = ClassLoader.getSystemResourceAsStream("sql_connect.properties");

            prop.load(input);
            propUser = prop.getProperty("DB_USER");
            propPasswd = prop.getProperty("DB_PASSWORD");

            if(propUser == null || propPasswd == null){
                throw new HomefinanceDaoException("cant read DB credentials from property file");
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        try {
            Connection connection = DriverManager.getConnection(dbUrl, propUser, propPasswd);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public Connection connect(String dbUrl, String dbUser, String dbPasswd) {
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPasswd);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new HomefinanceDaoException(e.getMessage());
        }
    }
}
