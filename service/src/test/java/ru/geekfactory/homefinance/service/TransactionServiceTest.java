package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.repository.TransactionRepository;
import ru.geekfactory.homefinance.service.converter.TransactionConverter;
import ru.geekfactory.homefinance.service.model.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionServiceTest {
    private static final MathContext BD_CONTEXT = new MathContext(2, RoundingMode.HALF_UP);
    TransactionRepository transactionRepository = mock(TransactionRepository.class);
    TransactionService transactionService = new TransactionService(transactionRepository);
    TransactionModelService transactionModelService = new TransactionModelService();
    TransactionConverter transactionConverter = new TransactionConverter();
    AccountModelService accountModelService = new AccountModelService();
    CurrencyModelService currencyModelService = new CurrencyModelService();
    CategoryTransactionModelService categoryTransactionModelService = new CategoryTransactionModelService();
    List<CategoryTransactionModelService> catList = new ArrayList<>();

    @BeforeEach
    private void prepareModels() {
        String date = "2017-03-08T12:30:54";
        LocalDateTime tstamp = LocalDateTime.parse(date);

        currencyModelService.setId(0L);
        currencyModelService.setName("Ruble");
        currencyModelService.setCode("RUR");
        currencyModelService.setCourseTo(BigDecimal.ONE);
        currencyModelService.setCurrencyTo(currencyModelService);
        accountModelService.setId(0L);
        accountModelService.setName("test acct");
        accountModelService.setAmount(new BigDecimal(4500, BD_CONTEXT));
        accountModelService.setCurrency(currencyModelService);
        categoryTransactionModelService.setId(0L);
        categoryTransactionModelService.setName("test category");
        catList = Arrays.asList(categoryTransactionModelService);
        transactionModelService.setId(0L);
        transactionModelService.setDirection(TransactionDirectionService.RECEIPT);
        transactionModelService.setTimeStamp(tstamp);
        transactionModelService.setCategory(catList);
        transactionModelService.setAccount(accountModelService);
        transactionModelService.setAmount(new BigDecimal(500));
    }

    @Test
    @DisplayName("Transaction service save test")
    public void saveTest() {
        long expected = transactionModelService.getId();

        when(transactionRepository.save(transactionConverter.toDao(transactionModelService))).
                thenReturn(transactionModelService.getId());
        Assertions.assertEquals(expected, transactionService.save(transactionModelService));
    }

    @Test
    @DisplayName("Transaction service findall test")
    public void findAllTest() {
        int i = 0;
        String date = "2017-03-08T12:30:54";
        LocalDateTime tstamp = LocalDateTime.parse(date);
        List<TransactionModel> transactionModelList = new ArrayList<>();
        List<TransactionModelService> expected = new ArrayList<>();
        when(transactionRepository.findAll()).thenReturn(transactionModelList);

        while (i < 3) {
            transactionModelService = new TransactionModelService();
            transactionModelService.setId((long) i);
            transactionModelService.setDirection(TransactionDirectionService.RECEIPT);
            transactionModelService.setTimeStamp(tstamp);
            transactionModelService.setCategory(catList);
            transactionModelService.setAccount(accountModelService);
            transactionModelService.setAmount(new BigDecimal(500));

            expected.add(transactionModelService);
            i++;
        }

        for (TransactionModelService x : expected) {
            transactionModelList.add(transactionConverter.toDao(x));
        }

        Assertions.assertEquals(expected, transactionService.getTotalTransaction());
    }

    @Test
    @DisplayName("Transaction service update test")
    public void updateTest() {
        when(transactionRepository.update(transactionConverter.toDao(transactionModelService))).
                thenReturn(true).thenReturn(false);

        Assertions.assertTrue(transactionService.update(transactionModelService));
        Assertions.assertFalse(transactionService.update(transactionModelService));
    }

    @Test
    @DisplayName("Account service update test")
    public void deleteTest() {
        when(transactionRepository.delete(0L)).thenReturn(true).thenReturn(false);

        Assertions.assertTrue(transactionService.delete(0L));
        Assertions.assertFalse(transactionService.delete(0L));

    }
}
