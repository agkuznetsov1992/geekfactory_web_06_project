package ru.geekfactory.homefinance.service.converter;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.service.model.CategoryTransactionModelService;

public class CategoryConverter {
    public CategoryTransactionModel toDao(CategoryTransactionModelService categoryTransactionModelService){
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setId(categoryTransactionModelService.getId());
        categoryTransactionModel.setName(categoryTransactionModelService.getName());

        return categoryTransactionModel;
    }

    public CategoryTransactionModelService toService(CategoryTransactionModel categoryTransactionModel){
        CategoryTransactionModelService categoryTransactionModelService = new CategoryTransactionModelService();
        categoryTransactionModelService.setId(categoryTransactionModel.getId());
        categoryTransactionModelService.setName(categoryTransactionModel.getName());

        return categoryTransactionModelService;
    }
}
