package ru.geekfactory.homefinance.web.servlet;

import ru.geekfactory.homefinance.dao.model.TransactionModel;
//import ru.geekfactory.homefinance.dao.repository.TransactionRepository;
import ru.geekfactory.homefinance.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransactionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TransactionService transactionService = new TransactionService(); //todo перенести TransactionService в поле сервлета
        List<TransactionModel> transactionModelList = transactionService.getTotalTransaction();
        String htmlTemplateBegin = "<html>" +
                "<head><title>Transaction list</title></head>" +
                "<body>";
        String htmlTemplateEnd = "</body></html>";
        resp.getOutputStream().write(htmlTemplateBegin.getBytes());

        for (TransactionModel x : transactionModelList) {
            resp.getOutputStream().write("<br>".getBytes());
            resp.getOutputStream().write(x.toString().getBytes());
        }

        resp.getOutputStream().write(htmlTemplateEnd.getBytes());
    }
}
