package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.*;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AccountRepositoryTest {
    public static final String DELETEALL = "DELETE from account_tbl;" +
            "DELETE from currency_tbl;";
    public static final MathContext BDCONTEXT = new MathContext(3, RoundingMode.HALF_UP);
    private DBConnectorInterface dbConnectorH2;

    @BeforeEach
    @AfterEach
    void clearTables() {
        dbConnectorH2 = new DBConnectorH2();
        Connection connection = dbConnectorH2.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETEALL)) {
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            String hfErrorMsg = "FAIL clearing H2 account table.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    @Test
    @DisplayName("Save and find operation testing")
    void saveAndFindTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        //todo разобраться с чертовым bigdecimal. Почему он периодически сваливается в e-notation? Почему округляет до невменяемых чисел (15.274 -> 15.3 при MathContext(3, RoundingMode.HALF_UP))?
        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(new BigDecimal(1.55, BDCONTEXT));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(new BigDecimal(1754.12).setScale(2, RoundingMode.HALF_UP));
//        accountModel.setAmount(new BigDecimal(15.274, BDCONTEXT));

        currencyRepository.save(currencyModel);

        Long newAccountId = accountRepository.save(accountModel);

        AccountModel accountModelReaded = accountRepository.findById(newAccountId);
        assertEquals(accountModel, accountModelReaded);
    }

    @Test
    @DisplayName("Save models with same ID testing")
    void saveDuplicateTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(BigDecimal.valueOf(1000.01));

        currencyRepository.save(currencyModel);
        accountRepository.save(accountModel);

        assertThrows(HomefinanceDaoException.class, () -> {
            accountRepository.save(accountModel);
        });
    }

    @Test
    @DisplayName("Find all operation testing")
    void findAllTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);
        List<AccountModel> accountModelList = new ArrayList<>();
        List<AccountModel> accountModelListReaded = new ArrayList<>();

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(new BigDecimal(1.55, BDCONTEXT));
        currencyModel.setCurrencyTo(null);
        currencyRepository.save(currencyModel);

        for (int x = 0; x < 10; x++) {
            AccountModel accountModel = new AccountModel();
            accountModel.setName("test H2 acct " + x);
            accountModel.setCurrency(currencyModel);
            accountModel.setAmount(new BigDecimal(1754.12).setScale(2, RoundingMode.HALF_UP));
            accountRepository.save(accountModel);
            accountModelList.add(accountModel);
        }

        accountModelListReaded = accountRepository.findAll();
        Assertions.assertEquals(accountModelList, accountModelListReaded);
    }

    @Test
    @DisplayName("Save model with empty fields testing")
    void saveEmptyFieldsTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(null);
        accountModel.setAmount(BigDecimal.valueOf(1000.01));

        assertThrows(HomefinanceDaoException.class, () -> {
            accountRepository.save(accountModel);
        });

        accountModel.setCurrency(currencyModel);
        accountModel.setName("");

        assertThrows(HomefinanceDaoException.class, () -> {
            accountRepository.save(accountModel);
        });
    }

    @Test
    @DisplayName("Update operation testing")
    void updateTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(BigDecimal.valueOf(1000.01));
        currencyRepository.save(currencyModel);

        Long newAccountId = accountRepository.save(accountModel);

        accountModel.setName("test acct renamed");
        accountRepository.update(accountModel);
        AccountModel accountModelReaded = accountRepository.findById(newAccountId);
        assertEquals(accountModel, accountModelReaded);
    }

    @Test
    @DisplayName("Update empty fields testing")
    void updateEmptyTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(BigDecimal.valueOf(1000.01));
        currencyRepository.save(currencyModel);

        accountRepository.save(accountModel);

        accountModel.setName("");
        assertThrows(HomefinanceDaoException.class, () -> {
            accountRepository.update(accountModel);
        });
    }

    @Test
    @DisplayName("Delete operation testing")
    void deleteTest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(BigDecimal.valueOf(1000.01));
        currencyRepository.save(currencyModel);

        Long newAccountId = accountRepository.save(accountModel);
        accountRepository.delete(newAccountId);
        AccountModel accountModelReaded = accountRepository.findById(newAccountId);

        assertNull(accountModelReaded);
    }

    @Test
    @DisplayName("Delete non-existing row operation testing")
    void deleteNETest() {
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);

        currencyModel.setCode("RUR");
        currencyModel.setName("Ruble");
        currencyModel.setCourseTo(BigDecimal.valueOf(1.01));
        currencyModel.setCurrencyTo(null);
        accountModel.setName("test acct");
        accountModel.setCurrency(currencyModel);
        accountModel.setAmount(BigDecimal.valueOf(1000.01));
        currencyRepository.save(currencyModel);

        accountRepository.save(accountModel);
        assertTrue(accountRepository.delete(accountModel.getId()));
        assertFalse(accountRepository.delete(accountModel.getId()));
    }
}
