package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;
import ru.geekfactory.homefinance.service.converter.AccountConverter;
import ru.geekfactory.homefinance.service.model.AccountModelService;

import java.util.ArrayList;
import java.util.List;

public class AccountService {
    AccountRepository accountRepository;
    AccountConverter accountConverter = new AccountConverter();

    public AccountService() {
        this.accountRepository = new AccountRepository();
    }

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public long save(AccountModelService accountModelService) {
        AccountModel convertedModel = accountConverter.toDao(accountModelService);

        return accountRepository.save(convertedModel);
    }

    public List<AccountModelService> getTotalAccounts() {
        List<AccountModelService> returnArray = new ArrayList<>();
        List<AccountModel> currencyModelList = accountRepository.findAll();

        for (AccountModel x : currencyModelList) {
            returnArray.add(accountConverter.toService(x));
        }

        return returnArray;
    }

    public boolean update(AccountModelService accountModelService) {
        AccountModel convertedModel = accountConverter.toDao(accountModelService);

        return accountRepository.update(convertedModel);
    }

    public boolean delete(long id) {
        return accountRepository.delete(id);
    }
}
