package ru.geekfactory.homefinance.dao.model;

import ru.geekfactory.homefinance.dao.repository.HomefinanceDaoException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class TransactionModel {
    private Long id;
    private AccountModel account;
    private BigDecimal amount;
    private List<CategoryTransactionModel> category;
    private TransactionDirection direction;
    private LocalDateTime timeStamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        try {
            this.amount = amount.setScale(2, ROUND_HALF_UP);
        } catch (NullPointerException exc) {
            String hfErrorMsg = "Model fields can't be empty.";
            throw new HomefinanceDaoException(hfErrorMsg);
        }

    }

    public List<CategoryTransactionModel> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryTransactionModel> category) {
        this.category = category;
    }

    public TransactionDirection getDirection() {
        return direction;
    }

    public void setDirection(TransactionDirection direction) {
        this.direction = direction;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "TransactionModel{" +
                "id=" + id +
                ", account=" + account.getName() +
                ", amount=" + amount +
                ", category=" + category.toString() +
                ", direction=" + direction.name() +
                ", timeStamp=" + timeStamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionModel)) return false;
        TransactionModel that = (TransactionModel) o;

        return Objects.equals(id, that.id) &&
                Objects.equals(account, that.account) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(category, that.category) &&
                direction == that.direction &&
                Objects.equals(timeStamp, that.timeStamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, amount, category, direction, timeStamp);
    }
}
