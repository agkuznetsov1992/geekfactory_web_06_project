package ru.geekfactory.homefinance.dao.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CategoryTransactionTest {
    private CategoryTransactionModel categoryTransactionModel;


    @BeforeEach
    void beforeEach() {
        categoryTransactionModel = new CategoryTransactionModel();
    }

    @Test
    @DisplayName("Id testing")
    void idTest() {
        assertNotNull(categoryTransactionModel);
        categoryTransactionModel.setId(6L);
        assertEquals(6L, categoryTransactionModel.getId().longValue());
    }

    @Test
    @DisplayName("Name field testing")
    void nameTest() {
        assertNotNull(categoryTransactionModel);
        categoryTransactionModel.setName("123456");
        assertEquals("123456", categoryTransactionModel.getName());
    }
}
