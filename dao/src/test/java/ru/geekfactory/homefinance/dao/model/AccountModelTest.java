package ru.geekfactory.homefinance.dao.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AccountModelTest {
    private AccountModel accountModel;

    @BeforeEach
    void beforeEach() {
        accountModel = new AccountModel();
    }

    @Test
    @DisplayName("Id field testing")
    void idTest() {
        assertNotNull(accountModel);
        accountModel.setId(6L);
        assertEquals(6L, accountModel.getId().longValue());
    }

    @Test
    @DisplayName("Name field testing")
    void nameTest() {
        assertNotNull(accountModel);

        String testName = "test account";
        accountModel.setName(testName);
        assertEquals(testName, accountModel.getName());
    }

    @Test
    @DisplayName("Amount field testing")
    void amountTest() {
        assertNotNull(accountModel);

        BigDecimal testAmount = new BigDecimal(10.5);
        accountModel.setAmount(testAmount);
        assertEquals(testAmount, accountModel.getAmount());
    }

    @Test
    @DisplayName("Currency field testing")
    void currencyTest() {
        assertNotNull(accountModel);

        CurrencyModel testCurrency = new CurrencyModel();
        accountModel.setCurrency(testCurrency);
        assertEquals(testCurrency, accountModel.getCurrency());
    }
}
