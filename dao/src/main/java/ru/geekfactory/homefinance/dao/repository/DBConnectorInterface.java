package ru.geekfactory.homefinance.dao.repository;

import java.sql.Connection;

public interface DBConnectorInterface {
    public Connection connect();
}
