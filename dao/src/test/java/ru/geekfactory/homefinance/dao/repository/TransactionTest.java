package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.*;
import ru.geekfactory.homefinance.dao.model.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TransactionTest {
    public static final String TRUNCATE = "TRUNCATE TABLE transaction_tbl";
    public static final String DELETEALL = "DELETE from transaction_categories_tbl;" +
            " DELETE from category_tbl;" +
            " DELETE FROM transaction_tbl;" +
            " DELETE FROM account_tbl;" +
            " DELETE FROM currency_tbl;";
    public static final MathContext BDCONTEXT = new MathContext(2, RoundingMode.HALF_UP);
    private DBConnectorInterface dbConnectorH2;
    private CategoryTransactionModel categoryTransactionModel;
    private AccountModel accountModel;
    private CurrencyModel currencyModel;

    @BeforeEach
    @AfterEach
    void clearTables() {
        dbConnectorH2 = new DBConnectorH2();
        Connection connection = dbConnectorH2.connect();

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETEALL)) {
            int affRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            String hfErrorMsg = "FAIL truncating H2 transaction table.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    private TransactionModel createTestTransaction() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String date = "2017-03-08T12:30:54";
        LocalDateTime tstamp = LocalDateTime.parse(date);

        categoryTransactionModel = new CategoryTransactionModel();
        accountModel = new AccountModel();
        currencyModel = new CurrencyModel();
        TransactionModel transactionModel = new TransactionModel();

        currencyModel.setName("Ruble");
        currencyModel.setCode("RUR");
        currencyModel.setCurrencyTo(null);
        currencyModel.setCourseTo(BigDecimal.ONE.setScale(2));
        currencyRepository.save(currencyModel);

        accountModel.setAmount(new BigDecimal(1000).setScale(2));
        accountModel.setCurrency(currencyModel);
        accountModel.setName("test account");
        accountRepository.save(accountModel);

        categoryTransactionModel.setName("testH2Category");
        categoryRepository.save(categoryTransactionModel);
        List<CategoryTransactionModel> catList = Arrays.asList(categoryTransactionModel);

        transactionModel.setDirection(TransactionDirection.RECEIPT);
        transactionModel.setTimeStamp(tstamp);
        transactionModel.setCategory(catList);
        transactionModel.setAccount(accountModel);
        transactionModel.setAmount(new BigDecimal(500));

        return transactionModel;
    }

    @Test
    @DisplayName("Find all transaction in single category testing")
    void findByCategoryTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);
        CategoryTransactionModel categoryExcluded = new CategoryTransactionModel();
        List<TransactionModel> expected = new ArrayList<>();
        TransactionModel transactionModel = createTestTransaction();

        categoryExcluded.setName("excluded category");
        categoryRepository.save(categoryExcluded);

        for (int x = 0; x < 5; x++) {
            TransactionModel transactionModelToWrite = new TransactionModel();

            transactionModelToWrite.setId(null);
            transactionModelToWrite.setAmount(new BigDecimal(10 * x, BDCONTEXT));
            transactionModelToWrite.setDirection(transactionModel.getDirection());
            transactionModelToWrite.setAccount(transactionModel.getAccount());
            transactionModelToWrite.setTimeStamp(transactionModel.getTimeStamp());
            if (x == 4) {
                transactionModelToWrite.setCategory(Arrays.asList(categoryExcluded));
            } else {
                transactionModelToWrite.setCategory(transactionModel.getCategory());
                expected.add(transactionModelToWrite);
            }
            transactionRepository.save(transactionModelToWrite);
        }

        long targetCategoryId = transactionModel.getCategory().get(0).getId();
        List<TransactionModel> actual = transactionRepository.findByCategory(targetCategoryId);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Save and find operation testing")
    void saveAndFindTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String date = "2017-03-08T12:30:54";
        LocalDateTime tstamp = LocalDateTime.parse(date);

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        TransactionModel transactionModel = new TransactionModel();

        currencyModel.setName("Ruble");
        currencyModel.setCode("RUR");
        currencyModel.setCurrencyTo(null);
        currencyModel.setCourseTo(BigDecimal.ONE.setScale(2));
        currencyRepository.save(currencyModel);

        accountModel.setAmount(new BigDecimal(1000).setScale(2));
        accountModel.setCurrency(currencyModel);
        accountModel.setName("test account");
        accountRepository.save(accountModel);

        categoryTransactionModel.setName("testH2Category");
        categoryRepository.save(categoryTransactionModel);
        List<CategoryTransactionModel> catList = Arrays.asList(categoryTransactionModel);

        transactionModel.setDirection(TransactionDirection.RECEIPT);
        transactionModel.setTimeStamp(tstamp);
        transactionModel.setCategory(catList);
        transactionModel.setAccount(accountModel);
        transactionModel.setAmount(new BigDecimal(500.01));

        long newTransactionId = transactionRepository.save(transactionModel);
        TransactionModel transactionModelReaded = transactionRepository.findById(newTransactionId);

        Assertions.assertEquals(transactionModel, transactionModelReaded);
    }

    @Test
    @DisplayName("Save models with same ID testing")
    void saveDuplicateTest() {
        TransactionModel transactionModel = createTestTransaction();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);

        transactionRepository.save(transactionModel);
        assertThrows(HomefinanceDaoException.class, () -> {
            transactionRepository.save(transactionModel);
        });
    }

    @Test
    @DisplayName("FindAll operation testing")
    void findAllTest() {
        TransactionModel transactionModel = createTestTransaction();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);
        List<TransactionModel> transactionModelList = new ArrayList<>();
        List<TransactionModel> transactionModelListReaded = new ArrayList<>();

        for (int x = 0; x < 5; x++) {
            TransactionModel transactionModelToWrite = new TransactionModel();

            transactionModelToWrite.setId(null);
            transactionModelToWrite.setAmount(new BigDecimal(10 * x, BDCONTEXT));
            transactionModelToWrite.setDirection(transactionModel.getDirection());
            transactionModelToWrite.setAccount(transactionModel.getAccount());
            transactionModelToWrite.setCategory(transactionModel.getCategory());
            transactionModelToWrite.setTimeStamp(transactionModel.getTimeStamp());
            transactionRepository.save(transactionModelToWrite);
            transactionModelList.add(transactionModelToWrite);
        }

        transactionModelListReaded = transactionRepository.findAll();

        Assertions.assertEquals(transactionModelList, transactionModelListReaded);
    }

    @Test
    @DisplayName("Save model with empty fields testing")
    void saveEmptyFieldsTest() {
        CategoryRepository categoryRepository = new CategoryRepository(dbConnectorH2);
        AccountRepository accountRepository = new AccountRepository(dbConnectorH2);
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnectorH2);
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String date = "2017-03-08T12:30:54";
        LocalDateTime tstamp = LocalDateTime.parse(date);

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        AccountModel accountModel = new AccountModel();
        CurrencyModel currencyModel = new CurrencyModel();
        TransactionModel transactionModel = new TransactionModel();

        currencyModel.setName("Ruble");
        currencyModel.setCode("RUR");
        currencyModel.setCurrencyTo(null);
        currencyModel.setCourseTo(BigDecimal.ONE);
        currencyRepository.save(currencyModel);

        accountModel.setAmount(new BigDecimal(1000));
        accountModel.setCurrency(currencyModel);
        accountModel.setName("test account");
        accountRepository.save(accountModel);

        categoryTransactionModel.setName("testH2Category");
        categoryRepository.save(categoryTransactionModel);
        List<CategoryTransactionModel> catList = Arrays.asList(categoryTransactionModel);

        transactionModel.setDirection(TransactionDirection.RECEIPT);
        transactionModel.setTimeStamp(tstamp);
        transactionModel.setCategory(catList);
        transactionModel.setAccount(accountModel);
        transactionModel.setAmount(new BigDecimal(500));

        transactionModel.setAccount(null);
        assertThrows(HomefinanceDaoException.class, () -> {
            transactionRepository.save(transactionModel);
        });

        transactionModel.setAmount(new BigDecimal(500));
        transactionModel.setTimeStamp(null);
        assertThrows(HomefinanceDaoException.class, () -> {
            transactionRepository.save(transactionModel);
        });

        transactionModel.setTimeStamp(tstamp);
        transactionModel.setDirection(null);
        assertThrows(HomefinanceDaoException.class, () -> {
            transactionRepository.save(transactionModel);
        });
    }

    @Test
    @DisplayName("Update operation testing")
    void updateTest() {
        TransactionModel transactionModel = createTestTransaction();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);

        transactionRepository.save(transactionModel);
        transactionModel.setAmount(new BigDecimal(400.01));
        transactionRepository.update(transactionModel);

        TransactionModel transactionModelReaded = transactionRepository.findById(transactionModel.getId());
        System.out.println(transactionModel.toString());
        assertEquals(transactionModel.toString(), transactionModelReaded.toString());
    }

    @Test
    @DisplayName("Update empty fields testing")
    void updateEmptyTest() {
        TransactionModel transactionModel = createTestTransaction();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);

        transactionRepository.save(transactionModel);

        transactionModel.setDirection(null);
        assertThrows(HomefinanceDaoException.class, () -> {
            transactionRepository.update(transactionModel);
        });
    }

    @Test
    @DisplayName("Delete operation testing")
    void deleteTest() {
        TransactionModel transactionModel = createTestTransaction();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);

        long newTransactionId = transactionRepository.save(transactionModel);
        transactionRepository.delete(newTransactionId);
        TransactionModel transactionModelReaded = transactionRepository.findById(newTransactionId);
        assertNull(transactionModelReaded);
    }

    @Test
    @DisplayName("Delete non-existing row operation testing")
    void deleteNETest() {
        TransactionModel transactionModel = createTestTransaction();
        TransactionRepository transactionRepository = new TransactionRepository(dbConnectorH2);

        long newTransactionId = transactionRepository.save(transactionModel);
        assertTrue(transactionRepository.delete(newTransactionId));
        assertFalse(transactionRepository.delete(newTransactionId));
    }
}
