package ru.geekfactory.homefinance.dao.model;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CurrencyTest {
    private CurrencyModel currencyModel;

    @BeforeEach
    void beforeEach() {
        currencyModel = new CurrencyModel();
    }

    @Test
    @DisplayName("Id field testing")
    void idTest() {
        assertNotNull(currencyModel);

        long testId = 6L;
        currencyModel.setId(testId);
        assertEquals(testId, currencyModel.getId().longValue());
    }

    @Test
    @DisplayName("Name field testing")
    void nameTest() {
        assertNotNull(currencyModel);

        String testName = "Ruble";
        currencyModel.setName(testName);
        assertEquals(testName, currencyModel.getName());
    }

    @Test
    @DisplayName("Code field testing")
    void codeTest() {
        assertNotNull(currencyModel);

        String testCode = "RUR";
        currencyModel.setCode(testCode);
        assertEquals(testCode, currencyModel.getCode());
    }

    @Test
    @DisplayName("CourseTo field testing")
    void courseToTest() {
        assertNotNull(currencyModel);

        BigDecimal testCourseTo = new BigDecimal(1.000);
        currencyModel.setCourseTo(testCourseTo);
        assertEquals(testCourseTo, currencyModel.getCourseTo());
    }

    @Test
    @DisplayName("CurrencyTo field testing")
    void currencyToTest() {
        assertNotNull(currencyModel);

        currencyModel.setCurrencyTo(currencyModel);
        assertEquals(currencyModel, currencyModel.getCurrencyTo());
    }

}
