CREATE TABLE IF NOT EXISTS currency_tbl (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  code VARCHAR(10) NOT NULL,
  course_to DECIMAL(15, 2) NOT NULL,
  currency_to_id INT,

  CONSTRAINT currency_to_fk FOREIGN KEY(currency_to_id) REFERENCES currency_tbl(id)
);

CREATE TABLE IF NOT EXISTS account_tbl (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  amount DECIMAL(15, 2) NOT NULL,
  currency_id INT NOT NULL,

  CONSTRAINT currency_fk FOREIGN KEY(currency_id) REFERENCES currency_tbl(id)
);

CREATE TABLE IF NOT EXISTS direction_tbl (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS transaction_tbl (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  account_id INT NOT NULL,
  amount DECIMAL(15, 2) NOT NULL,
  direction VARCHAR(10) NOT NULL,
  timestamp DATETIME ,

  CONSTRAINT account_fk FOREIGN KEY(account_id) REFERENCES account_tbl(id)
);


CREATE TABLE IF NOT EXISTS category_tbl (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS transaction_categories_tbl (
  category_id INT NOT NULL,
  transaction_id INT NOT NULL,

  CONSTRAINT category_fk FOREIGN KEY(category_id) REFERENCES category_tbl(id),
  CONSTRAINT transaction_fk FOREIGN KEY(transaction_id) REFERENCES transaction_tbl(id)
);
