package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.AccountModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountRepository {
    public static final String INSERT = "INSERT into account_tbl " +
            "(name, amount, currency_id) " +
            "VALUES (?, ?, ?)";
    public static final String SELECT = "SELECT id, name, amount, currency_id from account_tbl " +
            "WHERE id = ?";
    public static final String SELECTALL = "SELECT id, name, amount, currency_id from account_tbl"; //todo все константы переименовать в стиле FIND_ALL
    public static final String UPDATE = "UPDATE account_tbl SET " +
            "name = ?, amount = ?, currency_id =? " +
            "WHERE id = ?";
    public static final String DELETE = "DELETE FROM account_tbl " +
            "WHERE id = ?";
    private DBConnectorInterface dbConnector;

    public AccountRepository() {
        this.dbConnector = new DBConnector();
    }

    public AccountRepository(DBConnectorInterface dbConnector) {
        this.dbConnector = dbConnector;
    }

    private void modelFieldsCheck(AccountModel model) {
        if (model.getName() == null || model.getCurrency() == null || model.getName().equals("")) {
            String hfErrorMsg = "Model fields can't be empty.";
            throw new HomefinanceDaoException(hfErrorMsg);
        }
    }

    public Long save(AccountModel model) {
        Connection connection = dbConnector.connect();
        Long newAccountId = null;

        if (model.getId() != null) {
            if (findById(model.getId()) != null) {
                String hfErrorMsg = "Already have entry with same ID.";
                throw new HomefinanceDaoException(hfErrorMsg);
            }
        }

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setBigDecimal(2, model.getAmount());
            preparedStatement.setLong(3, model.getCurrency().getId());
            preparedStatement.executeUpdate();

            ResultSet queryResult = preparedStatement.getGeneratedKeys();

            if (queryResult.next()) {
                newAccountId = queryResult.getLong(1);
                model.setId(newAccountId);
            }

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL saving account to SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newAccountId;
    }

    public boolean update(AccountModel model) {
        Connection connection = dbConnector.connect();
        int affectedRows = 0;

        modelFieldsCheck(model);

        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setBigDecimal(2, model.getAmount());
            preparedStatement.setLong(3, model.getCurrency().getId());
            preparedStatement.setLong(4, model.getId());
            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL updating account in SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean delete(Long id) {
        Connection connection = dbConnector.connect();
        int affectedRows = 0;

        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setLong(1, id);
            affectedRows = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL deleting account from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }

        if (affectedRows == 1) {
            return true;
        } else {
            return false;
        }
    }

    public AccountModel findById(Long id) {
        Connection connection = dbConnector.connect();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnector);

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT)) {
            preparedStatement.setLong(1, id);
            ResultSet queryResult = preparedStatement.executeQuery();
            AccountModel returnValue = new AccountModel();

            if (queryResult.next()) {
                returnValue.setId(queryResult.getLong("id"));
                returnValue.setName(queryResult.getString("name"));
                returnValue.setAmount(queryResult.getBigDecimal("amount"));
                returnValue.setCurrency(currencyRepository.findById(queryResult.getLong("currency_id")));
            } else {
                return null;
            }

            return returnValue;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new HomefinanceDaoException(e1.getMessage(), e1.getCause());
            }

            String hfErrorMsg = "FAIL reading account from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }

    public List<AccountModel> findAll() {
        Connection connection = dbConnector.connect();
        CurrencyRepository currencyRepository = new CurrencyRepository(dbConnector);
        List<AccountModel> returnArray = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECTALL)) {
            ResultSet queryResult = preparedStatement.executeQuery();

            while (queryResult.next()) {
                AccountModel singleValue = new AccountModel();
                singleValue.setId(queryResult.getLong("id"));
                singleValue.setName(queryResult.getString("name"));
                singleValue.setAmount(queryResult.getBigDecimal("amount"));
                singleValue.setCurrency(currencyRepository.findById(queryResult.getLong("currency_id")));
                returnArray.add(singleValue);
            }

            return returnArray;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

            String hfErrorMsg = "FAIL reading currency from SQL.";
            throw new HomefinanceDaoException(hfErrorMsg + '\t' + e.getMessage());
        }
    }
}
