package ru.geekfactory.homefinance.dao.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TransactionModelTest {
    private TransactionModel transactionModel;

    @BeforeEach
    void beforeEch() {
        transactionModel = new TransactionModel();
    }

    @Test
    @DisplayName("Id field testing")
    void idTest() {
        assertNotNull(transactionModel);

        long testId = 6L;
        transactionModel.setId(testId);
        assertEquals(testId, transactionModel.getId().longValue());
    }

    @Test
    @DisplayName("Account field testing")
    void accountTest() {
        assertNotNull(transactionModel);

        AccountModel testAccount = new AccountModel();
        transactionModel.setAccount(testAccount);
        assertEquals(testAccount, transactionModel.getAccount());
    }

    @Test
    @DisplayName("Amount field testing")
    void amountTest() {
        assertNotNull(transactionModel);

        BigDecimal testAmount = new BigDecimal(10.5).setScale(2);
        transactionModel.setAmount(testAmount);
        assertEquals(testAmount, transactionModel.getAmount());
    }

    @Test
    @DisplayName("Amount field testing")
    void categoryTest() {
        assertNotNull(transactionModel);

        List<CategoryTransactionModel> testCategoryList = Arrays.asList(new CategoryTransactionModel(),
                new CategoryTransactionModel());
        transactionModel.setCategory(testCategoryList);
        assertEquals(testCategoryList, transactionModel.getCategory());
    }

    @Test
    @DisplayName("Direction field testing")
    void directionTest() {
        assertNotNull(transactionModel);

        transactionModel.setDirection(TransactionDirection.RECEIPT);
        assertEquals(TransactionDirection.RECEIPT, transactionModel.getDirection());
    }

    @Test
    @DisplayName("Data field testing")
    void dataTest() {
        assertNotNull(transactionModel);

        LocalDateTime testTimeStamp = LocalDateTime.of(2019, Month.MARCH, 28,
                16, 55, 06);
        transactionModel.setTimeStamp(testTimeStamp);
        assertEquals(testTimeStamp, transactionModel.getTimeStamp());
    }
}
