package ru.geekfactory.homefinance.dao.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnectorH2 implements DBConnectorInterface {
    public DBConnectorH2() {
        String propUrl = null;
        String propUser = null;
        String propPasswd = null;
        Properties prop = new Properties();

        try {
            File propFile = new File("src/test/resources/sql_connect.properties");
            InputStream input = new FileInputStream(propFile);

            prop.load(input);

            propUrl = "jdbc:h2:~/home_finance;INIT=runscript from 'src/test/resources/init_ddl.sql'\\;";
            propUser = prop.getProperty("DB_USER");
            propPasswd = prop.getProperty("DB_PASSWORD");

        } catch (IOException exc) {
            exc.printStackTrace();
        }

        try {
            Connection connection = DriverManager.getConnection(propUrl, propUser, propPasswd);
            connection.close();
        } catch (SQLException e) {
            throw new HomefinanceDaoException("H2 connector error: " + e.getMessage());
        }
    }

    public Connection connect() {
        String propUrl = null;
        String propUser = null;
        String propPasswd = null;
        Properties prop = new Properties();

        try {
            File propFile = new File("src/test/resources/sql_connect.properties");
            InputStream input = new FileInputStream(propFile);

            prop.load(input);

//            propUrl = "jdbc:h2:~/home_finance;INIT=runscript from 'src/test/resources/init_ddl.sql'\\;";
            propUrl = "jdbc:h2:~/home_finance";
            propUser = prop.getProperty("DB_USER");
            propPasswd = prop.getProperty("DB_PASSWORD");
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        try {
            Connection connection = DriverManager.getConnection(propUrl, propUser, propPasswd);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public Connection connect(String dbUrl) {
        String propUser = null;
        String propPasswd = null;
        Properties prop = new Properties();

        try {
            File propFile = new File("src/test/resources/sql_connect.properties");
            InputStream input = new FileInputStream(propFile);

            prop.load(input);
            propUser = prop.getProperty("DB_USER");
            propPasswd = prop.getProperty("DB_PASSWORD");

            if (propUser == null || propPasswd == null) {
                throw new HomefinanceDaoException("cant read DB credentials from property file");
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        try {
            Connection connection = DriverManager.getConnection(dbUrl, propUser, propPasswd);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new HomefinanceDaoException(e.getMessage());
        }
    }

    public Connection connect(String dbUrl, String dbUser, String dbPasswd) {
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPasswd);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new HomefinanceDaoException(e.getMessage());
        }
    }
}
