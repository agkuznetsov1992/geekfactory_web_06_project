package ru.geekfactory.homefinance.dao.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Optional;

@Getter
@Setter
public class CurrencyModel {
    private Long id;
    private String name;
    private String code;
    private BigDecimal courseTo;
    private CurrencyModel currencyTo;

    @Override
    public String toString() {
        Optional<CurrencyModel> optCTM = Optional.ofNullable(this.currencyTo);

        return "CurrencyModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", courseTo=" + courseTo +
                ", currencyTo=" + optCTM.map(CurrencyModel::getCode).orElse(this.getCode()) +
                '}';
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof CurrencyModel)) return false;
        final CurrencyModel other = (CurrencyModel) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CurrencyModel;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $code = this.getCode();
        result = result * PRIME + ($code == null ? 43 : $code.hashCode());
        final Object $courseTo = this.getCourseTo();
        result = result * PRIME + ($courseTo == null ? 43 : $courseTo.hashCode());
        final Object $currencyTo = this.getCurrencyTo();
        result = result * PRIME + ($currencyTo == null ? 43 : $currencyTo.hashCode());
        return result;
    }
}
