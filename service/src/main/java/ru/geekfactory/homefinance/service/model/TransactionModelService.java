package ru.geekfactory.homefinance.service.model;

import ru.geekfactory.homefinance.dao.repository.HomefinanceDaoException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class TransactionModelService {
    private Long id;
    private AccountModelService account;
    private BigDecimal amount;
    private List<CategoryTransactionModelService> category;
    private TransactionDirectionService direction;
    private LocalDateTime timeStamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountModelService getAccount() {
        return account;
    }

    public void setAccount(AccountModelService account) {
        this.account = account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        try {
            this.amount = amount.setScale(2, ROUND_HALF_UP);
        } catch (NullPointerException exc) {
            String hfErrorMsg = "Model fields can't be empty.";
            throw new HomefinanceDaoException(hfErrorMsg);
        }

    }

    public List<CategoryTransactionModelService> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryTransactionModelService> category) {
        this.category = category;
    }

    public TransactionDirectionService getDirection() {
        return direction;
    }

    public void setDirection(TransactionDirectionService direction) {
        this.direction = direction;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "TransactionModelService{" +
                "id=" + id +
                ", account=" + account.getName() +
                ", amount=" + amount +
                ", category=" + category.toString() +
                ", direction=" + direction.name() +
                ", timeStamp=" + timeStamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionModelService)) return false;
        TransactionModelService that = (TransactionModelService) o;

        return Objects.equals(id, that.id) &&
                Objects.equals(account, that.account) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(category, that.category) &&
                direction == that.direction &&
                Objects.equals(timeStamp, that.timeStamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, amount, category, direction, timeStamp);
    }
}
