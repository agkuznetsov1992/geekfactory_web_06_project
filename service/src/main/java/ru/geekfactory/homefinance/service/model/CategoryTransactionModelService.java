package ru.geekfactory.homefinance.service.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class CategoryTransactionModelService {
    private Long id;
    private String name;

    @Override
    public String toString() {
        return "CategoryTransactionModelService{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
