package ru.geekfactory.homefinance.dao.model;

public enum TransactionDirection {
    RECEIPT, SPEND;
}
