package ru.geekfactory.homefinance.dao.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class CategoryTransactionModel {
    private Long id;
    private String name;

    @Override
    public String toString() {
        return "CategoryTransactionModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
