package ru.geekfactory.homefinance.service.model;

public enum TransactionDirectionService {
    RECEIPT, SPEND;
}
